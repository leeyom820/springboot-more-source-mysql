package net.xdclass.demo2.controller;

import net.xdclass.dto.User;
import net.xdclass.demo2.mapper.master.MSQueryMapper;
import net.xdclass.demo2.mapper.other.OtherQueryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/test2")
public class UserController2 {

    @Autowired
    MSQueryMapper msq;

    @Autowired
    OtherQueryMapper oqm;

    @GetMapping("ms")
    public void msQuery(){
        List<User> users = msq.queryUserList();
        System.err.println(users);
    }

    @GetMapping("ss")
    public void sqlserverQuery(){
        List<User> users = oqm.queryUserList();
        System.err.println(users);
    }

}
