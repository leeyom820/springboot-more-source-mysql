package net.xdclass.demo2.mapper.master;

import net.xdclass.dto.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Repository 比 @Mapper+@Component好用
 */
@Repository
public interface MSQueryMapper {

    List<User> queryUserList();

}
