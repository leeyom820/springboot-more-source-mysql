package net.xdclass.demo2.mapper.other;

import net.xdclass.dto.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 */
@Repository
public interface OtherQueryMapper {

    List<User> queryUserList();

}
