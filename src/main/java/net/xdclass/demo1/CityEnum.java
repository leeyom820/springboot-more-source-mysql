package net.xdclass.demo1;

public enum CityEnum {
    /*武汉*/
    wuhan("wuhan"),
    /*恩施*/
    enshi("enshi");

    private String city;

    CityEnum(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }
}
