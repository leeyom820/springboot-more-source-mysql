package net.xdclass.demo1.mapper;

import net.xdclass.dto.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *  在使用SpringBoot整合Mybatis持久层框架时，对于Mybatis生成的接口类，例如 UserMapper，
 *  这些接口类 *Mapper在这里充当了Dao层的角色
 */
@Repository
public interface QueryMapper {

    List<User> queryUserList();

}
