package net.xdclass.demo1;

public class DataSourceHolder {
    //线程本地环境
    private static final ThreadLocal<String> contextHolders = new ThreadLocal<>();

    //设置数据源
    public static void setDataSource(String customerType) {
        contextHolders.set(customerType);
    }

    //获取数据源
    public static String getDataSource() {
        return contextHolders.get();
    }

    //清除数据源
    public static void clearDataSource() {
        contextHolders.remove();
    }

}
