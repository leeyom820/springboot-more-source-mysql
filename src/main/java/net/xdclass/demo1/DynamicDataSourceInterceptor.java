package net.xdclass.demo1;

import net.xdclass.demo1.dto.User;
import net.xdclass.demo1.mapper.QueryMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author best
 */
@Component
public class DynamicDataSourceInterceptor implements HandlerInterceptor {

    //在业务处理器处理请求之前被调用
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
//        String city = request.getHeader("city");
//        if (StringUtils.isBlank(city)) {
//            city = CityEnum.wuhan.getCity();
//        }
//        DataSourceHolder.setDataSource(city);
//        //RequestCityHolder.add(city);
//        System.err.println(city + "处理开始...");
        
        return true;
    }

    //请求完成之后
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
//        String city = request.getHeader("city");
//        if (StringUtils.isBlank(city)) {
//            city = CityEnum.wuhan.getCity();
//        }
//        //清除数据源
        DataSourceHolder.clearDataSource();
        //清除ThreadLocal变量
//        RequestCityHolder.remove();
//        System.err.println(city + "处理完成...");
        System.err.println( "处理完成...");
    }
}

