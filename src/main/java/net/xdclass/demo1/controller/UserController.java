package net.xdclass.demo1.controller;

import net.xdclass.demo1.DataSourceHolder;
import net.xdclass.dto.User;
import net.xdclass.demo1.mapper.QueryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/test")
public class UserController {

    @Autowired
    QueryMapper qp;

    @GetMapping("set")
    public void setDB(){
        DataSourceHolder.setDataSource("enshi");
        System.err.println(DataSourceHolder.getDataSource());
        List<User> users = qp.queryUserList();
        System.err.println(users);
    }

}
