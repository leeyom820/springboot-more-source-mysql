package net.xdclass;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 *  mapper包扫描注解: @MapperScan  并指定dao层接口的包路径
 *     * 扫描单个包: @MapperScan()
 *
 */
@SpringBootApplication
//@MapperScan("net.xdclass.demo3.mapper")   //扫描demo3里的mapper类
public class SpringbootDBApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDBApplication.class, args);
    }

}
