package net.xdclass.demo4.controller;

import net.xdclass.dto.Person;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 *
 *  controller层常用注解
 */
@RestController
@RequestMapping("/api/v1/anno")
public class AnnoController {

    /**
     *  访问: /api/v1/anno/test1?name=lilei
     *
     *
     */
    @GetMapping("test1")
    public String testAnno1(String name){
        System.err.println(name);
        return name;
    }

    /**
     *
     * 访问: /api/v1/anno/test2/2
     *   浏览器参数uid被注解@PathVariable里面变量接收, 所以两个参数名要一样, 注解接收到的参数在赋值给id
     */
    @GetMapping("test2/{uid}")
    public String testAnno2(@PathVariable(value="uid") String id){
        System.err.println(id);
        return id;
    }


    /**
     *
     * 访问: /api/v1/anno/test3?uname=lilei
     *
     */
    @GetMapping("test3")
    public String testAnno3(@RequestParam(name = "uname") String name){
        System.err.println(name);
        return name;
    }


    /**
     *  Body参数
     *    入参以JSON对象的方式传入：
     *      {
     *          "name":"suki_rong",
     *          "age":"18",
     *          "hobby":"programing"
     *      }
     *
     *      POST /api/v1/anno/test4
     *
     */
    @PostMapping(path = "test4")
    public void demo1(@RequestBody Person person) {
        System.err.println(person.toString());
    }


    /**
     *
     * form表单的形式提
     *
     *  post的请求，默认是formdata的方式传入的。如果是用formdata的方式传入的话，就无需任何注解，直接再方法中接受
     *
     *  http://localhost:9997/api/v1/anno/test5?name= uki_rong&age=81&hobby= programing
     *
     *  postman参数在Params里, 会追加到url
     *
     */
    @PostMapping(path = "test5")
    public void demo2(Person person) {
        System.err.println(person);
    }

    @GetMapping("/demo5")
    public void demo5(@RequestHeader(name = "myHeader") String myHeader,
                      @CookieValue(name = "myCookie") String myCookie) {
        System.out.println("myHeader=" + myHeader);
        System.out.println("myCookie=" + myCookie);
    }

    /**
     *  从header和cookie中取参数
     *    Spring MVC @RequestHeader注解用于将请求的头信息数据映射到功能处理方法的参数上
     *
     *    myHeader字段是在header头里自定义的
     */
    @GetMapping("test6")
    public void demo3(@RequestHeader(name = "myHeader") String myHeader,
                      @RequestHeader(name = "host") String host,
                      @RequestHeader(value="Accept")String[] accepts,
                      @RequestHeader("User-Agent")String userAgent) {
        System.out.println("myHeader=" + myHeader);
        System.out.println("host=" + host);
        System.out.println("Accept=" + accepts);
        System.out.println("User-Agent=" + userAgent);
    }

    @GetMapping("test8")
    public void demo4(@RequestHeader(value="Accept")String[] accepts,
                      @RequestHeader("User-Agent")String userAgent) {
        System.out.println("Accept=" + accepts);
        System.out.println("User-Agent=" + userAgent);
    }

    @GetMapping("test7")
    public void demo4(HttpServletRequest request) {
        System.out.println(request.getHeader("myHeader"));
        for (Cookie cookie : request.getCookies()) {
//            if ("myCookie".equals(cookie.getName())) {
                System.out.println(cookie.getValue());
//            }
        }
    }

}



