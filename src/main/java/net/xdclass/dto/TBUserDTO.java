package net.xdclass.dto;

import lombok.Data;

@Data
public class TBUserDTO {

    private Integer id;

    private String username;

    private String password;

    private String status;

    private String create_time;
}
