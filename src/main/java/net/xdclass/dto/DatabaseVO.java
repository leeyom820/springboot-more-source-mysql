package net.xdclass.dto;

import lombok.Data;

@Data
public class DatabaseVO {
    
    private String db;
    private String userId;
    private String uname;
}
