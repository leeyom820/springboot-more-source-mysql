package net.xdclass.dto;

import lombok.Data;
import java.util.List;

@Data
public class QueryParameterDTO {
 
    private List<String> ids; //传入的条件值1
 
    private List<Integer> statusList; //传入的条件值2
 
    // 前端传入的页码
    private int pageNo;  // 从1开始
 
    // 每页的条数
    private int pageSize;  //自己定
 
    // 数据库的偏移
//    private int offSet;
 
    // 数据库的大小限制
//    private int limit;
 
//    // 这里重写offSet和limit的get方法
//    public int getOffSet() {
//        return (pageNo-1)*pageSize;
//    }
// 
//    public int getLimit() {
//        return pageSize;
//    }
}
