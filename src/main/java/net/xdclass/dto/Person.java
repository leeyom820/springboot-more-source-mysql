package net.xdclass.dto;

import lombok.Data;

@Data
public class Person {
    
    private String name;
    private String age;
    private String hobby;
}
