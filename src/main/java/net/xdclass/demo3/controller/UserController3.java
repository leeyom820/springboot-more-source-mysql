package net.xdclass.demo3.controller;

import lombok.NonNull;
import net.xdclass.dto.*;
import net.xdclass.demo3.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api/v1/test3")
public class UserController3 {

    /**
     *  默认必须注入, 否则报错, 默认: @Autowired(required=true)
     *  @Autowired(required=false) 表示忽略当前要注入的 bean，如果有，直接注入；没有跳过，不会报错。
     */
//    @Autowired(required = false)
    @Autowired
    UserMapper um;

    @GetMapping("ms")
    public void msQuery(){
        List<User> users = um.queryUserList();
        System.err.println(users);
    }

    @GetMapping("page")
    public void getUserByLimit(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", 0); //起始位置
        map.put("pageSize", 2);  //页面大小
        List<User> users = um.getUserByLimit(map);
        System.err.println(users);
    }

    @GetMapping("index")
    public void queryMyApplicationRecord(){
        QueryParameterDTO qp = new QueryParameterDTO();
        qp.setPageNo(1); //前端传递的查询第几页
        qp.setPageSize(2); //一页显示多少条
        qp.setIds(Arrays.asList("1","2","3","4")); //第一个查询条件
        qp.setStatusList(Arrays.asList(0, 1));  //第二个查询条件
        List<TBUserDTO> records = um.queryMyApplicationRecord(qp);
        System.err.println(records);
    }

    @GetMapping("userid")
    public void getUserInfo(){
        Integer userId = 2;
        User user = um.getUserInfo(userId);
        System.err.println(user);
    }

    @GetMapping("userid2") //有问题, 位置参数
    public void getUserInfo2(){
        Integer userId = 2;
        String username = "lisi";
        User user = um.getUserInfo2(userId, username);
        System.err.println(user);
    }

    @GetMapping("userid3")
    public void getUserInfo3(){
        Integer userId = 2;
        String username = "lisi";
        User user = um.getUserInfo3(userId, username);
        System.err.println(user);
    }

    @GetMapping("userid4")
    public void getUserInfo4(){
        User2 user2 = new User2();
        user2.setUserId(2);
        user2.setUsername("lisi");
        User u = um.getUserInfo4(user2);
        System.err.println(u);
    }

    @GetMapping("userid5")
    public void getUserInfo5(){
        Map<String, Object> map = new HashMap<>();
        map.put("userId", 2);
        map.put("username", "lisi");
        User u = um.getUserInfo5(map);
        System.err.println(u);
    }

    @GetMapping("userid6")
    public void getUserInfo6(){
        Integer id = 2;
        String username = "lisi";
        User userInfo = new User(id, username);
        Map<String,Object> map = new HashMap<>();
        map.put("user",userInfo);
        User userResult = um.getUserInfo6(map);
        System.err.println(userResult);
    }

    @GetMapping("userid7")
    public void getUserInfo7(){
        Integer userId = 3;
        String username = "lisi";
        String uname = "wangwu";
        User user = new User(userId, username);
        User userResult = um.getUserInfo7(user, uname);
        System.err.println(userResult);
    }

    /**
     *
     * $｛｝
     *  使用这个的时候，只需要注意，如果是传递字段名或者表名，是直接做参数传入即可，
     *
     * 但是如果作为sql语句里面使用的值， 记得需要手动拼接 ' ' 号。
     *
     */
    @GetMapping("userid8")
    public void getUserInfo8(String name){
        String username = "'"+ name +"'"; //作为值查询, 使用$需要拼接单引号''包裹字符串
        User userResult = um.getUserInfo8(username);
        System.err.println(userResult);
    }

    @GetMapping("userid9")
    public void getUserInfo9(String name){
        Integer id = 2;
        String username = "'"+ name +"'";
        User userResult = um.getUserInfo9(id, username);
        System.err.println(userResult);
    }

    @GetMapping("userid10")
    public void getUserInfo10(){
        List<Integer> idsList = new ArrayList<>();
        idsList.add(1);
        idsList.add(2);
        List<User> userResult = um.getUserInfo10(idsList);
        System.err.println(userResult);
    }

    @GetMapping("userid11")
    public void getUserInfo11(){
        Map<String, Object> map = new HashMap();
        map.put("db", "wh");
        map.put("userId", 99);
        map.put("uname", "hm");
        User userResult = um.getUserInfo11(map);
        System.err.println(userResult);
    }

    @GetMapping("userid12")
    public void getUserInfo12(){
        DatabaseVO db = new DatabaseVO();
        db.setDb("wh");
        db.setUserId("99");
        db.setUname("hm");
        User userResult = um.getUserInfo12(db);
        System.err.println(userResult);
    }


    @GetMapping("vo")
    public void queryReUserDetail() throws ParseException {
        ReUserVo userVo = new ReUserVo();
        userVo.setUsername("lisi");
        String status = "1";
        DateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createTime = df.parse("2022-04-18 16:52:53");
        List<TBUserDTO> records = um.queryReUserDetail(userVo, createTime, status);
        System.err.println(records);
    }


}
