package net.xdclass.demo3.mapper;

import net.xdclass.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

 /**
 *
  * 扫描mapper类位置两种方式
 *    * 使用最多的是druid注入配置类扫描mapper位置
  *   * 直接在每个mapper类上加上注解: @Mapper和@Component
 *    * 直接在springboot启动类上写上扫描包:  @MapperScan("net.xdclass.demo3.mapper")
 * 
 */
@Mapper
@Component
public interface UserMapper {

    List<User> queryUserList();

     //简单分页查询
    List<User> getUserByLimit(Map<String, Integer> map);
    
    //真是场景分页, 实体类参数不使用注解@Param
    List<TBUserDTO> queryMyApplicationRecord(QueryParameterDTO qp);

    //传递 传入单个参数  userId
    User getUserInfo(Integer userId);

    //位置传递: 传入多个参数  userId, username  使用索引对应值
    User getUserInfo2(Integer userId, String username);

    //传入多个参数  userId,sex 使用注解@Param
    //在Mapper类中使用@Param("")注解，xml中使用的话，直接通过注解的名字找到。
    User getUserInfo3(@Param("userId")Integer userId, @Param("uname")String username);

    //传入多个参数  使用User实体类传入
    User getUserInfo4(User2 User);

    //传入多个参数， 使用Map类传入
    User getUserInfo5(Map<String, Object> map);

    //传入多个参，使用 map封装实体类传入, 这种情况其实使用场景比较少
    User getUserInfo6(Map map);

    //即需要传入实体类，又需要传入多个单独参，使用注解@Param
    User getUserInfo7(@Param("userInfo") User user, @Param("uname") String username);

    //传入单个参数 username
    User getUserInfo8(@Param("uname") String username);

    //传入多个单独参数 
    User getUserInfo9(@Param("userId") Integer userId, @Param("uname") String username);

    //传入一个list参数
    List<User> getUserInfo10(List<Integer> idList);

    //数据库是动态的
    User getUserInfo11(Map map);

    //数据库是动态的, 参数是实体类
    User getUserInfo12(DatabaseVO dbvo);

     //mapper里，使用注解@Param分被给实体类取名，以及给其他参数也取名：
    List<TBUserDTO> queryReUserDetail(@Param("reUserVo") ReUserVo reUserVo, 
                                                  @Param("createTime") Date createTime,
                                                  @Param("status") String status);



}
